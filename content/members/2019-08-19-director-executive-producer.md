---
member: member
name: Carlos Macher
title: Director & Executive Producer
Profile Image: src/images/CarlosMacher.jpg
Description: >-
 Carlos E. Macher is an award-winning filmmaker and actor from Lima, Peru. Based in Washington, DC. In the U.S. he has produced and directed several films including the documentaries "Luigi's Story: The A-Word" and "Third Act"
---

