import React from "react"

import styled from "styled-components"
import { motion, useMotionValue  } from "framer-motion"

import { _homeImageURL, _yellow, _carmine, _blue, _khaki } from "../theme"

export default () => {
    return (
        <Container id="section-2">
          <Title>Contact Us</Title>
          <Form method="POST" data-netlify="true">
      
            <label for="fname"> Name </label>
            <InputWrapper>
              <input type="text" id="fname" name="firstname" placeholder="First Name... " />
              <InputBorder />
            </InputWrapper>

            <InputWrapper>
              <input type="text" id="lname" name="lastname" placeholder="Last Name... "/>
              <InputBorder />
            </InputWrapper>

            <label for="email">Email</label>
            <InputWrapper>
             <input type="text" id="lname" name="lastname" placeholder="Your email.." />
             <InputBorder />
            </InputWrapper>


            <label for="country">Country</label>
            <select id="country" name="country">
              <option value="australia">Australia</option>
              <option value="canada">Canada</option>
              <option value="usa">USA</option>
            </select>
          
          <Wrapper>
            <label for="subject">Subject</label>
            <textarea id="subject" name="subject" placeholder="Write something.." style={{height: "200px"}}></textarea>
          </Wrapper>
          <Btn> 
            <input type="submit" value="Submit" />
          </Btn>
        </Form>
      </Container> 
    )
}

const Container = styled(motion.div)`
    border-radius: 5px;
    background: ${_carmine};
    padding: 20px
    display: flex;
    flex-direction: column;
    padding: 1em;
    width: 100%;
    height: 30em; 
    color: ${_blue};

`
const Title = styled.h3`
  place-self: center;
  font-size: 42px;
  font-family: 'Alegreya Sans';
  text-transform: uppercase;
  color: ${_blue};
`

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
  place-items: center center;

  
  label {
    place-self: start;
    margin-top: 0;
    font-size: 24px;
    color: ${_khaki};
  }

  textarea {
    width: 50vw;
    heght: 20%;
    place-self: center;
    background: transparent;
    border: 2px solid ${_yellow};
    margin: 1em;
  }

  & > * {
    margin: 2em 1em;
  }

`

const Wrapper = styled.div`
  display: flex;
  place-items: center;
  & > * {
    place-self: center;

  }
`

const InputWrapper = styled.div`
  input {
    max-height: 2.8em;
    background: transparent;
    border: none;
    color: ${_khaki};
    display: inine-block;
    z-index: 50

  }

  position: relative;
`

const InputBorder = styled(motion.div)`
  position: absolute;
  left: 0;
  bottom: 0;
  background: ${_yellow};
  z-index: 999;
  height: 2px;
  width: 100%;
`

const Btn = styled(motion.div)`
    text-align: center;
    font-family: "Open Sans";
    font-size: 17px;
    padding: calc(.5em + 1vw);
    background: ${_yellow};
    line-height: 10px;
    letter-spacing: 1px;
    color: ${_blue};
    border-radius: 2em;
    text-decoration: none;

    &:hover {
        cursor: pointer;
    }
    input {
      text-decoration: none;
      background: inherit;
      border: none;
      outline: none;
    }

`