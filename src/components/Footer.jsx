import React from "react";
import styled from "styled-components";
import { motion } from "framer-motion";
import {_blue, _khaki, _yellow} from "../theme"

export default () => {
  return  <Container>
            <Copyright> © 2019 Venezuela Walking for Freedom</Copyright>
            <Donate> Donate </Donate>
            <PrivacyPolicy> Privacy Policy </PrivacyPolicy>
            
            <Screenings>Screenings </Screenings>
            <Schedule>Schedule a Screening</Schedule>
            <Press>Press </Press>
        
          </Container>
};

const Container = styled(motion.section)`
  height: 5em;
  width: 100vw;

  background: ${_blue};

  scroll-snap-align: end;

  display: flex;
  justify-content: center;

  // max-height: 45em;
  color: ${_khaki};

 & > * {

  margin: .7em;
  display: flex;
  place-content: center center;
  height: 2em;
  padding: 1em;
  line-height: 5px;


 } 

`;

const Copyright = styled.div`

`

const Donate = styled.div`

`

const PrivacyPolicy = styled.div`

`

const Screenings = styled.div`

`

const Schedule = styled.div`

`
const Press = styled.div`

`
