---
member: member
name: Astrid Garcia
title: Producer
Profile Image: src/images/DSC07086.jpg
Description: >-
  Astrid García is a strategic communications consultant at the Inter-American
  Development Bank (IDB) in Washington DC. In this capacity, she leads and
  designs communication plans for the e- learning course program in edX, an
  open-source learning initiative founded by Harvard University and MIT.
---

