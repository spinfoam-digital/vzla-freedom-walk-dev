import React, { useState, useLayoutEffect } from "react";
import styled from "styled-components";
import { motion } from "framer-motion";
import path from "path"
//import { members } from "../data";

import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import useMedia from "../effects/useMedia"
//const image = require('./../')

import { _blue, _khaki, _yellow} from "../theme"

import Carlos from "../images/CarlosMacher.jpg"
import Juan from "../images/JH-5es.jpg"
import Astrid from "../images/DSC07086.jpg"
import Amelia from "../images/DSC07036.jpg"


const memberImages = [Juan, Carlos, Astrid, Amelia]


export default () => {
  const display = useMedia(["(max-width: 450px)"], ["none"], "inline-block")
  const imgOffset = useMedia(["(max-width: 500px)"], ["15%"], "-16%")



  const data = useStaticQuery(
    graphql`
    query MyQuery2 {
      allMarkdownRemark(filter: {frontmatter: {member: {eq: "member"}}}) {
        edges {
          node {
            frontmatter {
              title
              name
              Description
              Profile_Image
            }
          }
        }
      }
    }
    `
  )


  
  const members = data.allMarkdownRemark.edges.map(item => item.node.frontmatter)
// FIGURE OUT SMARTER WAY TO IMPORT IMAGES USING ABSOLUTE PATHS
//  const [memberImage, setImage] = useState(null) 
  

  let team = members.map((member, idx) => {
    let memberImage = memberImages[idx]

    return (
      <MemberContainer>
        <Name>{member.name}</Name>
        <Title>{member.title}</Title>
        <Preview style={{ display }}>{member.Description}</Preview>
        <Image style={{right: imgOffset, backgroundImage: `url(${memberImage})`}}/>
      </MemberContainer>
    );
  });

  return <GridContainer id="section-1">{team}</GridContainer>;
};

const GridContainer = styled(motion.section)`
  height: 770px;
  max-height: 1000px;
  width: 100%;

  background: ${_blue};
  color: ${_khaki};

  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-columns: repeat( auto-fit, minmax(30em, 1fr));
  grid-gap: 2.75em;

  overflow-x: auto;

  // max-height: 45em;
`;

const MemberContainer = styled(motion.div)`
  margin: 3em;
  padding: 1.8em;
  padding-top: 0;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  
  transform: translateX(-1em);

  position: relative;
  & > * {
  }
`;
const Name = styled(motion.h2)`
  place-self: start start;
  margin-top: none;
  font-family: "Alegreya Sans";
  font-size: 36px;
  letter-spacing: 2px;
  z-index: 99;
`;
const Title = styled(motion.h3)`
  transform: translate(-1.5em, -1.5em);
  font-family: "Merriweather";
  font-size: 16px;
  letter-spacing: 2px;
  width: 50%;
  z-index: 99;
  color: ${_yellow};
`;
const Preview = styled(motion.div)`
  padding: 1.5em;
  width: 75%;
  height: 15em;
  position: absolute;
  left: 0;
  top: 30%;

  font-size: 18px;
  line-height: 30px;

  font-family: "Open Sans";
  z-index: 99;

`;
const Image = styled(motion.div)`
  position: absolute;
  right: -16%;
  top: -5%;
  width: 60%;
  height: 120%;
  min-height: 15em;
  margin-right: 1em;
  //background: url("${({ image }) => image}");
  background-size: cover;

  border: 1px solid black;
  background-repeat: no-repeat;
`;
